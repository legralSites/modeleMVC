<?php
//define('SITE_ENGINE_VERSION','sid');
define('SITE_ENGINE_VERSION','v0.0.5-sid');
require('../release.php');
//  === bufferisation ====  //
ob_start();


//  ==== session ==== //
session_name('session');
session_start();
//session_regenerate_id();

if (isset($_GET['deconnect'])){
    $_SESSION = array();
    session_destroy();
}

//  ==== configuration du projet ==== //
require("./configLocal/config.php");


//  ==== constantes generales ==== //
//define('DATENOW',date('Y/m/d'));
$protocole=(isset($_SERVER["HTTPS"]))?'https://':'http://';
define('ARIANE_URI',$_SERVER["REQUEST_URI"]);
define('ARIANE_FULL',$protocole.$_SERVER["SERVER_NAME"].ARIANE_URI);
unset($protocole);


// = ======================= = //
// = gestion du mot de passe = //
// = ======================= = //
/* $P_ETAT:etat du password
 * -1: non demandé
 * 0 demander non fournis
 * 1: fournis mais incorrect
 * 2: fournis ok
 */
$P_ETAT=-1; // par defaut pas de mot de passe demandé

/* $isPassAsk: etat de la demande du password, demande faite par le routeur (ensemble ou une page)
 * 0: pas de demande de pawwd
 * 1: demande faite
 */
$isPassAsk=0;   // 1: si un routeur a demander un mot de passe (permet d'activer un passwd general



//  === recuperation du mdp ====  //
$P_GIVE=NULL;
if(isset($_POST['p'])){$P_GIVE=$_POST['p'];}
elseif (isset($_SESSION['p'])){$P_GIVE=$_SESSION['p'];}
define ('P_GIVE',$P_GIVE);
$_SESSION['p']=$P_GIVE;
unset($P_GIVE);

//  === mot de passe globale requis ====  //
//if (P_GIVE !== PWD ){$isPassAsk=1;}


//  === Librairies ====  //
require('vendors/gestLib/gestLib.php');

//  === gestion du cache ====  //
require('modeles/_modeleMVC/CachePSP.php');
$cachePSP=new CachePSP();

// cacheFile exist?
// oui->renvoyer le cache -> exit
// non->continuer (contruction du cache)

require('modeles/_modeleMVC/dev.php');
define('MINIMIZE_SUFFIXE',DEV_LVL>0?'':'.min');
//require('vendors/Notifs/Notifs.php');


//  === Dev ====  //
error_reporting(NULL);
if (DEV_LVL > 0)error_reporting(E_ALL);


// = ======== = //
// = dataBase = //
// = ======== = //
// geré par le controleur


// = ========= = //
// = variables = //
// = ========= = //

// - route - //
$route='page';  // ROUTE PRIMAIRE
$routeurFile=$route.'.php';

// - controleur - //
$controleurFile=NULL; // $controleurFile='page/accueil.php'; //controleur par default

// - page - //
$pageBase='vues/';
$isPageExist=1;
$pageDefault='accueil';
$pageAsk=$pageDefault;  // page demander (code GET)
$pagePath=$pageDefault; // chemin (relatif ou absolu) + nom +(NOT ext) du fichier a charger
$pageExt='.php';                // par defaut les pages sont php

// - html - //
$titleHTML=''; //titleHTML

// - restauration par SESSION - //
//if (isset($_SESSION['redir_route']))$route=$_SESSION['redir_route'];
//if (isset($_SESSION['redir_page']))$pageAsk=$_SESSION['redir_page'];


// - surcharge par GET: selection du routeur par priorité - //
if (isset($_GET['page']))       {$route='page';     $pageAsk=$_GET['page'];}
elseif (isset($_GET['serveur'])){$route='serveur';  $pageAsk=$_GET['serveur'];}
elseif (isset($_GET['typo']))   {$route='typo';     $pageAsk=$_GET['typo'];}
elseif (isset($_GET['legralLibs']))   {$route='legralLibs';     $pageAsk=$_GET['legralLibs'];}
else{
    // route inexistante: ne fonctionnent pas dans ce systeme (la pageDefault de la 1ere route est appellé)
}

// - constantes (pré-route) - //
define ('ROUTE',$route);
define ('ROUTEUR_FILE',ROUTE.'.php');
define ('PAGE',$pageAsk);   // avant le routeur


// = ======= = //
// = routeur = //
// = ======= = //
$pageBase='vues/'.ROUTE.'/';
require('routeurs/'.ROUTE.'/'.ROUTEUR_FILE);


// = ============================== = //
// = gestion des pages inexistantes = //
// = ============================== = //

// -- detecté par le routeur -- //
define ('PAGE_EXIST',$isPageExist);
if (PAGE_EXIST === 0){ // surcharger la page '_pageNoExist' defini par le routeur
    $pageBase='vues/';
    $pagePath='page/_pageNoExist';    
    $pageExt='.html';    
}
 

// -- fichier inexistant -- //
define ('FICHIER_PATH',$pageBase.$pagePath.$pageExt);
define ('FICHIER_EXIST',file_exists(FICHIER_PATH));
if ( FICHIER_EXIST === FALSE){
    $pageBase='vues/';
    $pagePath='page/_pageNoExist';    // racine = ./vues/
    $pageExt='.html';
}
define ('PAGE_BASE',$pageBase);


// - constantes (post-route) - //
define ('CONTROLEUR_FILE',$controleurFile);
define ('PAGE_PATH',$pagePath);
define ('PAGE_EXT',$pageExt);

define('TITLEHTML',$titleHTML);
unset($titleHTML,$route,$routeurFile,$controleurFile,$pageBase,$pageDefault,$pageAsk,$pagePath,$pageExt,$ariane,$pspV);

// - si pas de password demandé: sauvegarde de la route et de la page  - //
if ($P_ETAT === -1){
    $_SESSION['redir_route']=ROUTE;
    $_SESSION['redir_page']=PAGE;
}


// = ========== = //
// = controleur = //
// = ========== = //
if (CONTROLEUR_FILE !== NULL)require('controleurs/'.CONTROLEUR_FILE);


// = ==== = //
// = vues = //
// = ==== = //


try{
    if (PAGE === 'export'){
        // - format special pas de HTML - //
        include ('vues/'.ROUTE.'/export.php');
    }
    else{
        require ('vues/index.php');
    }
}catch (Exception $e){

    echo gestLib_inspect('$e',$e);
    echo 'Exception reçue : ',  $e->getMessage(), "\n";
}




