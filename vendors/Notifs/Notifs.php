<?php
/*
 * class Notifs
 */
define ('NOTIFS_VERSION','0.0.1');

define ('NOTIF_MAIRIE_ADD',1);
define ('NOTIF_MAIRIE_UPDATE',2);
define ('NOTIF_MAIRIE_DELETE',3);


class Notifs{
    protected $txt;

   const ADD = 1;
   const UPDATE = 2;
   const DELETE = 3;
   const PWD_INVALIDE =101;

    function __construct(){
        $this->txt=array();
    }

    function nb(){
        return count($this->txt);
    }

    function set($id,$txt){
        $this->txt[$id]=$txt;
    }

    function del($id){
        unset($this->txt[$id]);
    }

    function get($id){
        return $this->txt[$id];
    }
    function getAll(){
        return $this->txt;
    }

    function is($id){
        return isset($this->txt[$id])?1:0;
    }

}

