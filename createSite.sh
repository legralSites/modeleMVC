#!/bin/sh

echo "createSite.sh v0.0.1"

echo "Copie les fichiers du modeles dans le repertoire courants"

modeleRoot="/www/git/sites/_modeleMVC/sid";

# #################### #
# functions du projet  #
# #################### #
evalCmd (){
    cmd=$1;
    echo $CMD $cmd $NORMAL;
    eval $cmd;
}

linker(){
    source="$1";
    dest="$2"

    # source existe PAS -> on sort
    if [ ! -e "$source" ];then
        echo "$WARN la source $source existe pas!$NORMAL";
        return
    fi

    # dest existe -> on sort
    if [ -e "$dest" ];then
        echo "$NORMAL la destination $dest existe $NORMAL";
        return
    fi
    evalCmd "link $source $dest";
}

lier(){
    source="$1";
    dest="$2"

    # source existe PAS -> on sort
    if [ ! -e "$source" ];then
        echo "$WARN la source $source existe pas!$NORMAL";
        return
    fi

    # source existe PAS -> on sort
    if [ ! -e "$source" ];then
        echo "$WARN la source $source existe pas!$NORMAL";
        return
    fi
    # dest existe -> on sort
    if [ -e "$dest" ];then
        echo "$NORMAL la destination $dest existe $NORMAL";
        return
    fi
    evalCmd "ln -s $source $dest";
}



evalCmd "cp --no-clobber $modeleRoot/../release.modele.php release.php"
echo ""
evalCmd "mkdir sid"
evalCmd "cd sid"
echo ""
evalCmd "cp --no-clobber $modeleRoot/README.modele README"
evalCmd "cp --no-clobber $modeleRoot/release.modele release"
echo ""
evalCmd "cp --no-clobber $modeleRoot/index-https.php index-https.php"
evalCmd "cp --no-clobber $modeleRoot/managesite.conf.sh managesite.conf.sh"
echo ""
evalCmd "cp --no-clobber $modeleRoot/index-https.php index-https.php"
evalCmd "cp --no-clobber $modeleRoot/index.php index.php"

echo ""
evalCmd "cp -R $modeleRoot/configLocal ./"

echo ""
evalCmd "mkdir -p menus/ routeurs/ controleurs/ modeles/ vues/"

echo ""
echo "Pages communes"
evalCmd "cp --no-clobber $modeleRoot/vues/index.php vues/index.php"
evalCmd "cp --no-clobber $modeleRoot/vues/footer.php vues/footer.php"


echo ""
evalCmd "mkdir -p vendors/"

echo ""
evalCmd "mkdir -p web/"
evalCmd "cp -R $modeleRoot/web/img web/img"

echo ""
evalCmd "mkdir -p web/css/"
lier /www/git/sites/lib/legral/css/ web/css-legral

evalCmd "mkdir -p web/js/"
lier /www/git/sites/lib/legral/js/ web/js-legral

evalCmd "mkdir -p web/locales/"

echo ""
echo "# - lib PHP: gestLib - #"
lib='gestLib';libV="$lib.php";    evalCmd "mkdir -p ./vendors/$lib;";
linker /www/git/sites/lib/legral/php/$lib/$libV	./vendors/$lib/$libV;


echo ""
echo "# - page: route d'exemple - #"
route="page";
lier $modeleRoot/menus/$route menus/$route
lier $modeleRoot/routeurs/$route routeurs/$route
lier $modeleRoot/vues/$route vues/$route


echo ""
evalCmd "git init"
echo ""
echo "Editer managesite.conf.sh" 
echo "managesite.sh --link --min"
echo ""








