#!/bin/sh
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier


#############
# variables #
# surcharge #
#############
outFileCSS='styles';
outFileJS='scripts';

#######################################
# linkFiles()                         #
# link (ou copie) des fichiers (lib)  #
#######################################
linkFiles(){
	echo "$INFO # - managesite.conf.sh:linkFiles() - #$NORMAL";

    ################################
    # - creation des repertoires - #
    ################################
    evalCmd "mkdir -p vendors"
    evalCmd "mkdir -p web/js web/css web/locales"


    ############
    # - CSS  - #
    ############
    
    # -- css: legral -- #
    echo "";
    echo "$INFO lier le repertoire des styles legral $NORMAL";
    lier /www/git/sites/lib/legral/css ./web/css-legral;


    # -- css: specifique au projet -- #


    # - librairies PHP & JS - #
    echo "";
	echo "$INFO # -- librairies PHP & JS -- # $NORMAL";

    
    ######################
    # - librairies: JS - #
    ######################
    echo "$INFO # - librairies: JS - # $NORMAL";

    echo "$INFO # -- librairies JS : legral: -- # $NORMAL";
    lib="gestLib";libV="$lib.js";         evalCmd "mkdir -p ./web/js-legral/$lib;"; evalCmd "linker /www/git/sites/lib/legral/php/$lib/$libV	./web/js-legral/$lib/$libV;";
    lib="modeleMVC";libV="$lib.js";       evalCmd "mkdir -p ./web/js-legral/$lib;"; evalCmd "linker /www/git/sites/lib/legral/js/$lib/$libV	./web/js-legral/$lib/$libV;";

    lib="cookies";libV="$lib.js";         evalCmd "mkdir -p ./web/js-legral/$lib;"; evalCmd "linker /www/git/sites/lib/legral/js/$lib/$libV ./web/js-legral/$lib/$libV;";
    lib="gestBufCirc";libV="$lib.js";     evalCmd "mkdir -p ./web/js-legral/$lib;"; evalCmd "linker /www/git/sites/lib/legral/js/$lib/$libV ./web/js-legral/$lib/$libV;";
    lib="gestClasseurs";libV="$lib.js";   evalCmd "mkdir -p ./web/js-legral/$lib;"; evalCmd "linker /www/git/sites/lib/legral/js/$lib/$libV ./web/js-legral/$lib/$libV;";
    lib="gestCycles";libV="$lib.js";      evalCmd "mkdir -p ./web/js-legral/$lib;"; evalCmd "linker /www/git/sites/lib/legral/js/$lib/$libV ./web/js-legral/$lib/$libV;";
    lib="gestionScenario";libV="$lib.js"; evalCmd "mkdir -p ./web/js-legral/$lib;"; evalCmd "linker /www/git/sites/lib/legral/js/$lib/$libV ./web/js-legral/$lib/$libV;";


    echo "$INFO # -- librairies tiers:js -- # $NORMAL";
    lib="modeleMVC";libV="$lib.js";       evalCmd "mkdir -p ./web/js-legral/$lib;";  evalCmd "linker /www/git/sites/lib/legral/js/$lib/$libV	./web/js-legral/$lib/$libV;";
    lib="md5";  libV="$lib-v2.2.min.js";  evalCmd "mkdir -p ./web/js-legral/crypt;"; evalCmd "linker /www/git/sites/lib/tiers/js/crypt/$libV ./web/js-legral/crypt/$libV;";
    lib="SHA-1";libV="$lib-v2.2.min.js";  evalCmd "mkdir -p ./web/js-legral/crypt;"; evalCmd "linker /www/git/sites/lib/tiers/js/crypt/$libV ./web/js-legral/crypt/$libV;";


    #######################
    # - librairies: PHP - #
    #######################
    echo "$INFO # - librairies: PHP - # $NORMAL";


    # ------------------- #
    echo "$INFO # -- librairies PHP : legral- # $NORMAL";
	lib='gestLib';libV="$lib.php";    evalCmd "mkdir -p ./vendors/$lib;";
    linker /www/git/sites/lib/legral/php/$lib/$libV	./vendors/$lib/$libV;
	lib='Notifs'; libV="$lib.php";    evalCmd "mkdir -p ./vendors/$lib;";
    linker /www/git/sites/lib/legral/php/$lib/$libV	./vendors/$lib/$libV;


    # ------------------- #
    echo "$INFO # -- librairies PHP : tiers -- # $NORMAL";

    #echo "$INFO  $NORMAL";


    ########################
    # - fichiers communs - #
    ########################
	echo "";
	echo "$INFO # -- fichiers communs -- # $NORMAL";
	#piwik.php


    ############################
    # - librairies du projet - #
    ############################
    echo "";
	echo "$INFO # -- librairies du projet -- # $NORMAL";


    #################################
    # - lier des ROUTES externes  - #
    #################################
    echo "";
	echo "$INFO # -- lier des ROUTES externes  -- # $NORMAL";

    evalCmd "mkdir -p menus/ routeurs/ controleurs/ modeles/ vues/"

    route='_modeleMVC';
    lier   /www/git/sites/_modeleMVC/sid/modeles/$route            modeles/$route;  #repertoire
    
    route='serveur';
    echo "$route"
    lier /www/git/sites/_modeleMVC-extras/routeurs/$route     routeurs/$route; #1 fichier
    lier   /www/git/sites/_modeleMVC-extras/controleurs/$route  controleurs/$route;  #repertoire
    lier   /www/git/sites/_modeleMVC-extras/menus/$route        menus/$route;  #repertoire
    lier   /www/git/sites/_modeleMVC-extras/vues/$route         vues/$route;  #repertoire

    route='typo';
    echo "$route"
    lier /www/git/sites/_modeleMVC-extras/routeurs/$route     routeurs/$route; #1 fichier
   #lier   /www/git/sites/_modeleMVC-extras/controleurs/$route  controleurs/$route;  #repertoire
    lier   /www/git/sites/_modeleMVC-extras/menus/$route        menus/$route;  #repertoire
    lier   /www/git/sites/_modeleMVC-extras/vues/$route         vues/$route;  #repertoire

    route='legralLibs';
    echo "$route"
    lier /www/git/sites/_modeleMVC-extras/routeurs/$route     routeurs/$route; #1 fichier
    lier   /www/git/sites/_modeleMVC-extras/menus/$route        menus/$route;  #repertoire
    #lier   /www/git/sites/_modeleMVC-extras/vues/$route        vues/$route;  #repertoire

    echo""
    lier   /www/git/sites/_modeleMVC-extras/vues/gestCycles     vues/gestCycles;  #repertoire
    lier   /www/git/sites/_modeleMVC-extras/vues/gestClasseurs  vues/gestClasseurs;  #repertoire
    unset $route;



    ################################
    # - Changement des droits  - #
	echo "";
	echo "$INFO Changement des droits $NORMAL";
	f='./';	cmd="chmod -R 755 $f"; evalCmd "$cmd";
	#f='./';	cmd="chown -R pascal:www-data $f"; evalCmd "$cmd";
}


#########################################################
# local_concatCSS|JS()                                  #
# concatenne les fichiers css dans ./styles/styles.css  #
# concatenne les fichiers js  dans ./locales/scripts.js #
#########################################################
# - concatenation des fichiers css - #
local_concateCSS() {
    echo "$couleurINFO # - concatenation des fichiers css via concateCSS - #$couleurNORMAL";

    f='./web/css-legral/knacss/knacss-V5.0.1.css';		    evalCmd "concateCSS $f";
    f='./web/css-legral/html/mvc1.css';		                evalCmd "concateCSS $f";
    f='./web/css-legral/menuStylisee/ms.css';		        evalCmd "concateCSS $f";

    f='./web/css-legral/notes/notesMVC.css';       	        evalCmd "concateCSS $f";
    f='./web/css-legral/tutoriels/tutorielsMVC.css';        evalCmd "concateCSS $f";
    
    # - feuille de style de librairie - "
    f='./web/css-legral/gestLib/gestLib.css';               evalCmd "concateCSS $f";
    f='./web/css-legral/notifs/notifs.css';		            evalCmd "concateCSS $f";
    f='./web/css-legral/gestClasseurs/gestClasseurs.css';   evalCmd "concateCSS $f";

} #local_concateCSS


# - concatenation des fichiers js - #
local_concateJS() {
    echo "$couleurINFO # - concatenation des fichiers js via concateJS - #$couleurNORMAL";
    #echo "$couleurWARN Pas de lib js à concatener $couleurNORMAL";return 1;	# a commenter une fois parametré

    # - librairies legral: js  - #
    f='./web/js-legral/gestLib/gestLib.js';                 evalCmd "concateJS $f";
    f='./web/js-legral/modeleMVC/modeleMVC.js';             evalCmd "concateJS $f";

    f='./web/js-legral/cookies/cookies.js';                 evalCmd "concateJS $f";
    f='./web/js-legral/gestBufCirc/gestBufCirc.js';         evalCmd "concateJS $f";
    f='./web/js-legral/gestClasseurs/gestClasseurs.js';     evalCmd "concateJS $f";
    f='./web/js-legral/gestCycles/gestCycles.js';           evalCmd "concateJS $f";
    f='./web/js-legral/gestionScenario/gestionScenario.js'; evalCmd "concateJS $f";
#    f='./web/js-legral/gestAncres/gestAncres.js';           evalCmd "concateJS $f";



    # - librairies tiers: js  - #
    f='./web/js-legral/crypt/md5-v2.2.min.js';              evalCmd "concateJS $f";
    f='./web/js-legral/crypt/SHA-1-v2.2.min.js';            evalCmd "concateJS $f";

    # - librairies du projet: js  - #

} #local_concateJS


