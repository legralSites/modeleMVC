<!DOCTYPE html><html lang="fr">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="author" content="Pascal TOLEDO">
    <meta name="generator" content="vim">
    <meta name="identifier-url" content="http://legral.fr">
    <meta name="date-creation-yyyymmdd" content="20170720">
    <meta name="date-update-yyyymmdd" content="20160720">
    <meta name="revisit-after" content="never">
    <meta name="category" content="">
    <meta name="publisher" content="legral.fr">
    <meta name="copyright" content="pascal TOLEDO">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo TITLEHTML;?></title>
    <?php //echo $meta;unset($meta);?>

    <!-- scripts et styles -->
<?php
    echo '<script src="./web/locales/scripts'.MINIMIZE_SUFFIXE.'.js"></script>';
    echo '<link rel="stylesheet" href="./web/locales/styles'.MINIMIZE_SUFFIXE.'.css" media="all" />';
?>
    <script>
    </script>

</head>

<body>
<div id="page">

    <!-- header -->
    <div id="header">
        <div id="headerGauche"></div>
        <h1><a href="https://legral.fr/_modeleMVC">_modeleMVC</a> - <?php echo SITE_ENGINE_VERSION?></h1>

        <!-- menus -->
        <nav id="nav">
<?php 
include 'menus/page/page.php';
include 'menus/serveur/serveur.php';
include 'menus/typo/typo.php';
include 'menus/legralLibs/legralLibs.php';
?>
        </nav>

        <div id="headerDroit">

<?php
    if (SERVER_RESEAU === 'LOCALHOST'){
        echo selectDev();
        echo selectDebug();
    } 
    echo selectVersion($siteVersion_release);
?>
        - <a href="?deconnect=1"><img src="web/img/deconnect-61x61.png" alt="deconnect" style="height:1em;vertical-align:middle;"/></a>
                <!--a href="?about=accueil">&agrave; propos de...</a-->
            </div>
        </div><!-- header -->
<?php
/*
    if (FICHIER_EXIST === FALSE){
        include 'vues/'.PAGE_PATH.PAGE_EXT;
    }
    else{
        include 'vues/'.ROUTE.'/'.PAGE_PATH.PAGE_EXT;
    }
 */
    include PAGE_BASE.PAGE_PATH.PAGE_EXT;

    include 'vues/footer.php';
?>
</div><!-- //page -->

<script>
gestAncres();
</script>
<a id="end"></a>
</body></html>
