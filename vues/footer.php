<!-- footer -->
<!-- footer gauche-->
<div id="footer">
<div id="footerGauche"><br />
<?php
define('DUREE',microtime(TRUE)-SCRIPTDEBUT);
if(( defined('VERSIONSTATIQUE')) AND (VERSIONSTATIQUE === 1) ){
    echo 'version statique(g&eacute;n&eacute;r&eacute;e le: '.date('d/m/Y').')';
}
else{
    echo 'page g&eacute;n&eacute;r&eacute; en '. number_format(DUREE,9).'s';
}
?><br />
    <a target="w3c" href="http://validator.w3.org/check?uri=<?php echo ARIANE_FULL?>%2F&charset=%28detect+automatically%29&doctype=Inline&group=0"><img style="height:10px;" alt="validation 3wc" src="web/img/w3c.png" /></a>

</div><!-- footer gauche: fin-->

<!-- footer centre -->
<span class="licence">
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" src="web/img/licenceCCBY-88x31.png"></a><br>
    <a xmlns:dct="http://purl.org/dc/terms/" href="https://legral.fr" rel="dct:source">http://legral.fr</a>.</span>
<!-- footer centre: fin -->

<!-- footer droit -->
<div id="footerDroit"><br />
<?php
if (DEV_LVL > 0){
    echo 'serveur r&eacute;seau:'.SERVER_RESEAU.'<br>';
    echo 'serveur nom:'.SERVER_NAME.'<br>';
    echo 'IP:'.SERVER_ADDR.'<br>';
}
?>
</div><!-- footer droit: fin -->

</div><!-- footer -->
<section id="dev">

<?php
//php include './piwik.php';
//
if (DEBUG_LVL > 0){
    echo '<br />';
    echo gestLib_inspect('ROUTE',ROUTE);
    echo gestLib_inspect('ROUTEUR_FILE',ROUTEUR_FILE);
    echo gestLib_inspect('CONTROLEUR_FILE',CONTROLEUR_FILE);
    echo gestLib_inspect('PAGE_BASE',PAGE_BASE);
    echo gestLib_inspect('PAGE',PAGE);
    echo gestLib_inspect('PAGE_PATH',PAGE_PATH);

    echo gestLib_inspect('FICHIER_EXIST',FICHIER_EXIST);
    echo gestLib_inspect('FICHIER_PATH',FICHIER_PATH);
    
    echo gestLib_inspect('$P_ETAT',$P_ETAT);


//    echo gestLib_inspect('ARIANE',ARIANE);

    echo gestLib_inspectOrigine('$_GET',$_GET);
    echo gestLib_inspectOrigine('$_POST',$_POST);
    echo gestLib_inspectOrigine('$_SESSION',$_SESSION);

//echo gestLib_inspect('notifs',$notifs->getAll());
    if (isset($ppo))    echo gestLib_inspectOrigine('$ppo-table()',$ppo->table());
    if (isset($ppo))    echo gestLib_inspect('$ppo-sqls()',$ppo->sqls());

    //echo '<hr />';
    echo $gestLib->tableau();
    echo '<script>document.write(gestLib.tableau());</script>';
}
?>
</section>
