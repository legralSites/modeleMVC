<?php
define('SITE_CONFIG',1);

// - Moteur du site - //
if (defined('SITE_ENGINE_ROOT') === FALSE) define('SITE_ENGINE_ROOT','./');
define('SITE_ENGINE_VERSION_ROOT',SITE_ENGINE_ROOT.SITE_ENGINE_VERSION.'/');				


// - Themes - //
define('LOGOSITE_SRC','web/img/logo-site.png');

define('IMGEDIT_SRC','web/img/edit.png.png');

define('IMG_EDITNC','<img src="'.IMGEDIT_SRC.'" style="width:20px;" alt="éditer" title="éditer" ');
define('IMG_EDIT',IMG_EDITNC.'/>');

// - general - //
define ('PWD_SERVEUR','mot_de_passe');

