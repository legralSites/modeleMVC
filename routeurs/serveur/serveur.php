<?php
// - route par defaut - //
//$controleurFile=ROUTE.'.php';
$controleurFile=NULL;
$titleHTML='serveur';
$pageExt='.php';


// - gestion du passwd pour cette route - //

$P_ETAT = P_GIVE === PWD_SERVEUR?2:0;   // - mdp ok OU activation de la demande du mdp - //


if ($P_ETAT < 2){
    $_SESSION['redir_route']=ROUTE;
    $_SESSION['redir_page']=PAGE;
    $pagePath='passAsk';
    $titleHTML='mot de passe requis';
}
else{
    // - Surcharge - //
    switch (PAGE){
        case 'passAsk':
            $titleHTML='mot de passe requis';
        $pagePath=PAGE;
            break;

        case 'phpinfo':
            $titleHTML='phpInfo';
            $pagePath=PAGE;
            break;

        case 'variablesPHP':
            $titleHTML='Les variables PHP';
            $pagePath=PAGE;
            break;
    
        case 'dbTest':
            $titleHTML='Test de connexion à une db';
            $controleurFile=ROUTE.'/'.PAGE.'.php';
            $pagePath=PAGE;
            break;

        //default: // PAGE inexistante
            //les pages inexistante ne sont pas gerer (zone protegée)
   }// switch ($pageAsk)

} //
