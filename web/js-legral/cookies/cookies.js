function Tcookies(){
	this.prefixe='';	// prefixe le nom des cookies;
	return null;
}


Tcookies.prototype={
	set:function(nom, valeur, expires){
		document.cookie=nom+"="+escape(valeur)+((expires==null) ? "" : ("; expires="+expires.toGMTString()));
	}

	,explode:function(offset){
		var endstr=document.cookie.indexOf (";", offset);
		if (endstr==-1) endstr=document.cookie.length;
		return unescape(document.cookie.substring(offset, endstr)); 
	}

	,get:function(nom){
		var arg=nom+"=";
		var alen=arg.length;
		var clen=document.cookie.length;
		var i=0;
		while (i<clen){
			var j=i+alen;
			if (document.cookie.substring(i, j)==arg)
				return this.explode(j);
			i=document.cookie.indexOf(" ",i)+1;
			if (i==0) break;
			}
		return null; 
	}
} // Tcookies.prototype

// - exemples d'utilisation - //

// -- creer -- //
//date=new Date;
//cookie= new Tcookies();
//date.setMonth(date.getMonth()+1); // expire dans un mois
//cookie.set("deja_venu", "oui", date);
//
// -- lire -- //
//deja_venu = cookie.get("deja_venu");
