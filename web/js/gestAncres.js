// #################### # //
// # gestion des ancres # //
// #################### # //

/*  - openTag: - 
* execute des actions (notament ouvrir des divs) selon l'ancre
*/
function gestAncres(){
    hash=location.hash.substring(1,location.hash.length);//suppression du '#'

    // - action selon le nom complet de l'ancre - //
    // - utiliser pour la demande de suppression (retourner a l'element a supprimer) - //
    switch (hash){
        case 'block':
            blockVisible(hash);         // rendre visible le block cible
            blockVisible('block_parent');    // rendre visible le block parent
            return null;
            break;
        case 'blockSet':
            blockVisible(hash);         // rendre visible le block cible
            blockVisible('block_parent');    // rendre visible le block parent
            return null;
            break;
    }

    // - suppression des numeros pour les ancres de 9 caracteres (eg: block_Set1) - //
    switch (ancre=hash.substring(0,9)){

        // - mairies - //
        case 'block_Set':
            blockNone('block_a_cacher');
            blockVisible(ancre);    // rendre visible le block parent
            blockVisible(hash);    // rendre visible le block cible
            //document.getElementById("monElement").classList.add('');// appliquer le style selection
            return ancre;
            break;
    
        case 'block_Add':
            blockNone('block_a_cacher');
            blockVisible(ancre);    // rendre visible le block parent
            blockVisible(hash);    // rendre visible le block cible
            //document.getElementById("monElement").classList.add('');// appliquer le style selection
            return ancre;
            break;

    }//switch (ancre=hash.substring(0,9))
}//openTag()

